<div id="TOC">
<ul>
<li><a href="#i.-hymn-to-lord-vasudeva.-शरवसदवषटकम">I. Hymn to Lord Vasudeva. (श्रीवासुदेवाष्टकम्)</a></li>
<li><a href="#ii.-to-real-wisdom.-परबधपञचक">II. To Real Wisdom. (प्रबोधपञ्चक</a></li>
<li><a href="#iii.-shlokas-for-morning-prayer-परत-समरणम">III. Shlokas for Morning Prayer (प्रातः स्मरणम्)</a></li>
<li><a href="#iv.-surrender-absolute-शरणगत">IV. Surrender Absolute (शरणागतिः)</a></li>
<li><a href="#v.-the-best-way-to-salvation.-परममकषमरगनरपणम">V. The Best Way to Salvation. (परममोक्षमार्गनिरूपणम्)</a></li>
<li><a href="#vi.-the-profound-influence-of-सधसङग-सधसङगमहतमयम">VI. The Profound Influence of साधुसङ्ग (साधुसङ्गमाहात्म्यम्)</a></li>
<li><a href="#vii.-the-glories-of-a-bhakta.-भकतमहतमयम">VII. The Glories of a Bhakta. (भक्तमाहात्म्यम्)</a></li>
<li><a href="#viii.-the-glory-of-bhakti.-भकतमहतमयम">VIII. The Glory of Bhakti. (भक्तिमाहात्म्यम्)</a></li>
<li><a href="#ix.-surrender-absolute-at-sri-haris-lotus-feet.-भगवतपरपतत">IX. Surrender Absolute at Sri Hari’s Lotus Feet. (भगवत्प्रपत्तिः)</a></li>
<li><a href="#x.-the-hymn-of-an-akinchan.-अकञचनसतत">X. The Hymn of an Akinchan. (अकिञ्चनस्तुतिः)</a></li>
<li><a href="#xi.-hymn-sung-by-bhaktas.-भकतगतम">XI. Hymn sung by Bhaktas. (भक्तगीतम्)</a></li>
<li><a href="#xii.-repulsion-of-the-fear-of-hell.-नरकभय-नवरणम">XII. Repulsion of the Fear of Hell. (नरकभय-निवारणम्)</a></li>
<li><a href="#xiii.-how-the-sin-of-kali-is-warded-off.-कलकलमषनशनम">XIII. How the Sin of Kali is Warded off. (कलिकल्मषनाशनम्)</a></li>
<li><a href="#xiv.-the-mystery-of-the-universe-तततवरहसयम">XIV. The Mystery of the Universe (तत्त्वरहस्यम्)</a></li>
<li><a href="#xv.-vasudeva-geetam.-वसदवगतम">XV. Vasudeva Geetam. (वासुदेवगीतम्)</a></li>
<li><a href="#xvi.-in-praise-of-achar.-आचरमहतमयम">XVI. In Praise of Achar. (आचारमाहात्म्यम्)</a></li>
<li><a href="#xvii.-expiation-for-sins-committed.-परयशचततनरपणम">XVII. Expiation for sins committed. (प्रायश्चित्तनिरूपणम्)</a></li>
<li><a href="#xviii.-the-highest-wisdom.-परमतततव-कथनम">XVIII. The Highest Wisdom. (परमतत्त्व-कथनम्)</a></li>
<li><a href="#xix.-how-to-get-rid-of-maya-मयनरसनम">XIX. How to get Rid of Maya (मायानिरसनम्)</a></li>
<li><a href="#xx.-the-path-to-salvation.-मकषक-सधनम">XX. The Path to Salvation. (मोक्षैक-साधनम्)</a></li>
<li><a href="#xxi.-indication-of-sadhus.-सध-लकषणम">XXI. Indication of Sadhus. (साधु-लक्षणम्)</a></li>
<li><a href="#xxii.-singing-the-glories-of-sri-bhagawan.-भगवदगणनवद-महतमयम">XXII. Singing the Glories of Sri Bhagawan. (भगवद्गुणानुवाद-माहात्म्यम्)</a></li>
<li><a href="#xxiii.-the-glory-of-the-dust-of-the-feet-of-saints.-महतपदरज-महतमय">XXIII. The Glory of the Dust of the Feet of Saints. (महत्पादरजो-माहात्म्य</a></li>
<li><a href="#xxiv.-what-is-bhagawat-dharma.-भगवतधरमम-नरपणम">XXIV. What is Bhagawat Dharma. (भागवतधर्म्म-निरूपणम्)</a></li>
<li><a href="#xxv.-prapanna-geetam-muchukundas-prayer-to-srikrishna.-परपननगतम">XXV. Prapanna Geetam (Muchukunda’s Prayer to SriKrishna.) (प्रपन्नगीतम्)</a></li>
<li><a href="#xxvi.-prayer-of-one-who-yearns-for-salvation.-ममकषसतत">XXVI. Prayer of one who yearns for Salvation. (मुमुक्षुस्तुतिः)</a></li>
<li><a href="#xxvii.-brahma-stuti-or-prayer-of-brahma.-सकषपत-बरहमसतत">XXVII. Brahma Stuti or Prayer of Brahma. (संक्षिप्त-ब्रह्मस्तुतिः)</a></li>
<li><a href="#xxviii.-miscellaneous.-परकरणधयय">XXVIII. Miscellaneous. (प्रकीर्णाध्यायः)</a></li>
</ul>
</div>
